# Bienvenue dans le projet FAQ

## Serveur

* `Django`
* `REST`
* `SQLite`

### Configuration initiale

```
cd server/faq
pip install -r requirements.txt
python manage.py makemigrations
python manage.py migrate
python manage.py createsuperuser
```

### Lancement

```
python manage.py runserver
```

> On peut éventuellement gérer la base de données depuis 
[DjangoAdmin](http://localhost:8000/admin) 
en utilisant l'identifiant et le mot de passe **superuser**.

## Web

* `Vue`
* `Vuex`
* `Vuetify`

### Configuration initiale
```
cd client/web
yarn install
cp .env.example .env
yarn serve
```

### Lancement
```
yarn serve
```
