from django.urls import path
from .views import questions_list, questions_detail, answers_detail

urlpatterns = [
    path('api/v1/questions/', questions_list),
    path('api/v1/questions/<int:pk>/', questions_detail),
    path('api/v1/answers/<int:pk>/', answers_detail),
]
