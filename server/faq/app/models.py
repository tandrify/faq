from django.db import models

# Create your models here.

class Question (models.Model):
    content = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.content

    class Meta:
        ordering = ['-created_at']

class Answer (models.Model):
    content = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    question = models.ForeignKey(Question, related_name="answers", on_delete=models.CASCADE, null=True)

    def __str__(self):
        return self.content

    class Meta:
        ordering = ['-created_at']
