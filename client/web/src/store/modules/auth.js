const TOKEN_KEY = 'FAQ_TOKEN';
const TOKEN_VALUE = '123456789';

export const auth = {
    namespaced: true,
    state: {
        token: localStorage.getItem(TOKEN_KEY)
    },
    getters: {
        getToken: (state) => state.token,
    },
    mutations: {
        setToken: (state, token) => (state.token = token),
    },
    actions: {
        login: async ({commit}) => {
            return new Promise(resolve => {
                localStorage.setItem(TOKEN_KEY, TOKEN_VALUE);
                commit('setToken', TOKEN_VALUE);
                setTimeout(() => resolve(TOKEN_VALUE), 500);
            });
        },

        logout: async ({commit}) => {
            return new Promise(resolve => {
                localStorage.removeItem(TOKEN_KEY);
                commit('setToken', null);
                setTimeout(resolve, 500);
            });
        },
    },
}
