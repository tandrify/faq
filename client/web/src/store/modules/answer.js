import axios from "axios";

export const answer = {
    namespaced: true,
    actions: {
        sendAnswer: async ({commit, rootGetters}, data) => {
            if (!data) return;
            const {questionId, ...other} = data;
            try {
                const res = await axios.post(`${process.env.VUE_APP_QUESTIONS_URL}${questionId}/`, {...other});
                if ((res?.status ?? 400) === 201) {
                    const questions = rootGetters["question/getQuestions"];
                    const _questions = [...questions];
                    const i = _questions.findIndex(question => question.id === questionId);
                    const updatedAnswers = [res?.data, ..._questions[i].answers];
                    const updatedQuestion = {..._questions[i], answers: updatedAnswers};
                    _questions.splice(i, 1, updatedQuestion);
                    commit("question/setQuestions", _questions, {root: true});
                }
            } catch (e) {
                console.error(`Erreur de l'envoi de la réponse : ${e}`);
            }
        },
    }
}
