import axios from "axios";

export const question = {
    namespaced: true,
    state: {
        questions: [],
    },
    getters: {
        getQuestions: (state) => state.questions,
    },
    mutations: {
        setQuestions: (state, payload) => (state.questions = [...(payload ?? [])]),
    },
    actions: {
        fetchQuestions: async ({commit}) => {
            try {
                const res = await axios.get(process.env.VUE_APP_QUESTIONS_URL || "");
                commit("setQuestions", res?.data);
            } catch (e) {
                console.error(`Erreur lors de la récupération des questions : ${e}`);
            }
        },

        sendQuestion: async ({commit, state}, data) => {
            if (!data) return;
            const {content} = data;
            try {
                const res = await axios.post(process.env.VUE_APP_QUESTIONS_URL || "", {content});
                if ((res?.status ?? 400) === 201) {
                    commit("setQuestions", [res?.data, ...state.questions]);
                }
            } catch (e) {
                console.error(`Erreur de l'envoi de la question : ${e}`);
            }
        },
    },
}
