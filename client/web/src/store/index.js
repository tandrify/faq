import Vue from 'vue'
import Vuex from 'vuex'
import {question} from "@/store/modules/question";
import {answer} from "@/store/modules/answer";
import {auth} from "@/store/modules/auth";

Vue.use(Vuex)

export default new Vuex.Store({
    modules: {
        question,
        answer,
        auth,
    }
})
